//
//  RPSCameraConstants.swift
//  Roposo
//
//  Created by Shashank Agarwal on 07/02/18.
//  Copyright © 2018 Roposo. All rights reserved.
//

import Foundation
import UIKit
import CoreAudio

struct RPSCameraConstants {
    static let kRPSVideoConfigurationDefaultBitrate = 4000000
    static let kRPSAudioConfigurationDefaultBitrate = 128000
    static let kRPSCameraAudioFormat = kAudioFormatMPEG4AAC
    static let kRPSAudioConfigurationDefaultNumberOfChannels = 2
    static let kRPSAudioConfigurationDefaultSampleRate = 44100
    static let kRPSCameraOutputSize = CGSize(width: 720, height: 1280)

}
